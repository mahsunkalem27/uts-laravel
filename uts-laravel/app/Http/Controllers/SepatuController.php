<?php

namespace App\Http\Controllers;

use App\Models\sepatu;
use Illuminate\Http\Request;

class SepatuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = sepatu::all();

        return view('sepatu.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\sepatu  $sepatu
     * @return \Illuminate\Http\Response
     */
    public function show(sepatu $sepatu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\sepatu  $sepatu
     * @return \Illuminate\Http\Response
     */
    public function edit(sepatu $sepatu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\sepatu  $sepatu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, sepatu $sepatu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\sepatu  $sepatu
     * @return \Illuminate\Http\Response
     */
    public function destroy(sepatu $sepatu)
    {
        //
    }
}
