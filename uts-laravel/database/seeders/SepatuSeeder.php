<?php

namespace Database\Seeders;

use App\Models\sepatu;
use Illuminate\Database\Seeder;

class SepatuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new sepatu();

        $data->create([
            'merk' => 'adidas',
            'ukuran' => 40,
        ]);
    }
}
